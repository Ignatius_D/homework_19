﻿#include <iostream>

class Animal {
public:
    virtual void voice() = 0;
    ~Animal(){
        std::cout << "Class deleted!" << std::endl;
    }
};

class Dog :public Animal {
public:

    virtual void voice() {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat :public Animal {
public:
    virtual void voice() {
        std::cout << "Meow!" << std::endl;
    }
};

class Cow :public Animal {
public:
    virtual void voice() {
        std::cout << "Moo!" << std::endl;
    }
};

int main() {
    Animal* arr[4];
    arr[0] = new Cat();
    arr[1] = new Dog();
    arr[2] = new Cow();
    arr[3] = new Dog();
    for (auto it:arr)
        it->voice();
   
    for (auto it : arr)
        delete it;
}
